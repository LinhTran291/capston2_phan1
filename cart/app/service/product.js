function ProductService() {
    this.productListArray = [];

    // Trả về đối tượng promise
    this.getProductList = function () {
        return axios({
        method: "get",
        url: "https://647f229bc246f166da90243e.mockapi.io/product",
        });
    };

    // Thêm product
    this.addProduct = function (product) {
        return axios({
        method: "post",
        url: "https://647f229bc246f166da90243e.mockapi.io/product",
        data: product,
        });
    };

    // Cập nhật product
    this.updateProduct = function (product, id) {
        return axios({
        method: "put",
        url: `https://647f229bc246f166da90243e.mockapi.io/product/${id}`,
        data: product,
        });
    };

    this.deleteProduct = function (id) {
        return axios({
            method: "delete",
            url: `https://647f229bc246f166da90243e.mockapi.io/product/${id}`,
        });
    };

    // Lấy thông tin product theo ID
    this.getProductDetail = function (id) {
        return axios({
        method: "get",
        url: `https://647f229bc246f166da90243e.mockapi.io/product/${id}`,
        });
    };
}